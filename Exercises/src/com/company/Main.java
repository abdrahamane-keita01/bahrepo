package com.company;

import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        // write your code here

        int[] arrayNum = {1, 1, 2, 4, 5, 5, 6, 6, 9, 9};

        Set<Integer> numbs = new HashSet<Integer>();
        for (int i = 0; i < arrayNum.length; i++) {
            if (numbs.contains(arrayNum[i])) 
                continue;

                int count = 1;
                for (int j = i + 1; j < arrayNum.length; j++) {
                    if (arrayNum[i] == arrayNum[j]) {
                        count++;

                    }
                }
                System.out.println("The number " + arrayNum[i] + " is repeated " + count + " Time");
                numbs.add(arrayNum[i]);


            }
        }
    }

